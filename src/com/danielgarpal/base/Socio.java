package com.danielgarpal.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

public class Socio {

    private ObjectId id;
    private String nombre;
    private String dni;
    private LocalDate fechaNacimiento;
    private LocalDate fechaRegistro;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public LocalDate getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(LocalDate fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Override
    public String toString() {
        return "Id: " + id +
                " - Nombre: " + nombre + '\'' +
                " - DNI:  " + dni + '\'' +
                " - Fecha Nacimiento: " + fechaNacimiento +
                " - Fecha Registro: " + fechaRegistro;
    }
}

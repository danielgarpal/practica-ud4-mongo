package com.danielgarpal.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

public class Compra {


    private ObjectId id;
    private LocalDate fechaCOmpra;
    private String producto;
    private String precio;
    private Socio socio;


    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public LocalDate getFechaCOmpra() {
        return fechaCOmpra;
    }

    public void setFechaCOmpra(LocalDate fechaCOmpra) {
        this.fechaCOmpra = fechaCOmpra;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }


    public Socio getSocio() {
        return socio;
    }

    public void setSocio(Socio socio) {
        this.socio = socio;
    }

    @Override
    public String toString() {
        return "Id: " + id +
                " - Fecha compra: " + fechaCOmpra +
                " - Producto :" + producto + '\'' +
                " - Precio: " + precio + '\'' +
                " - Socio: " + socio.getNombre();
    }
}

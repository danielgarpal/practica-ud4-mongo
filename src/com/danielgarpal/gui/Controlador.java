package com.danielgarpal.gui;

import com.danielgarpal.base.Compra;
import com.danielgarpal.base.Socio;
import com.danielgarpal.util.Util;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.util.List;

public class Controlador implements ActionListener {
    Vista vista;
    Modelo modelo;

    //Constructor de la clase controlador
    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;
        iniciar();
    }

    //metodo que inicia la funcionalidad del controlador
    private void iniciar() {

        addActionListener(this);
        modelo.conectar();
        listarCompras(modelo.getCompras());
        listarSocios(modelo.getSocios());

    }

    //Añado los listener y los comandos de accion a los botones
    private void addActionListener(ActionListener listener) {
        vista.sociosAniadir.addActionListener(listener);
        vista.sociosModificar.addActionListener(listener);
        vista.sociosEliminar.addActionListener(listener);
        vista.comprasAniadir.addActionListener(listener);
        vista.comprasModificar.addActionListener(listener);
        vista.comprasEliminar.addActionListener(listener);

        vista.sociosAniadir.setActionCommand("socAniadir");
        vista.sociosModificar.setActionCommand("socMod");
        vista.sociosEliminar.setActionCommand("socElim");
        vista.comprasAniadir.setActionCommand("comAniadir");
        vista.comprasModificar.setActionCommand("comMod");
        vista.comprasEliminar.setActionCommand("comELim");
    }

    /*
    Metodo que se ejecuta cuando se activa el listener, y realiza la funcionalidad principal del programa
    (Añadir, modificar, eliminar)
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        switch (comando){
            /*
            Añadir socio, comprueba que todos los campos estan rellenados y crea un nuevo socio en la base de datos
             */
            case "socAniadir":

                if (!vista.sociosNombre.getText().equals("") || !vista.sociosDni.getText().equals("") ||
            !vista.sociosFechaNacimiento.getText().equals("") || !vista.sociosFechaRegistro.getText().equals("")){
                try {

                    //Crea un nuevo objeto de socio y le añade los datos
                    Socio socio = new Socio();
                    socio.setDni(vista.sociosDni.getText());
                    socio.setNombre(vista.sociosNombre.getText());
                    socio.setFechaNacimiento(vista.sociosFechaNacimiento.getDate());
                    socio.setFechaRegistro(vista.sociosFechaRegistro.getDate());
                    //Manda el objeto al método que lo guarda en la base de datos
                    modelo.guardarSocio(socio);
                    //Lista los socios en la tabla y en el combo box
                    listarSocios(modelo.getSocios());


                }catch(NumberFormatException ne){
                Util.showErrorAlert("Introduce números en los campos que lo requieran");
            }
            }else{
                Util.showErrorAlert("no puedes dejar campos vacíos");
            }
                break;
                //Método que modifica un socio
            case "socMod":
                if (!vista.sociosNombre.getText().equals("") || !vista.sociosDni.getText().equals("") ||
                        !vista.sociosFechaNacimiento.getText().equals("") || !vista.sociosFechaRegistro.getText().equals("")){
                    try {

                        //Crea un nuevo objeto de socio y le introduce el socio seleccionado en la lista
                        Socio socio = vista.listaSocios.getSelectedValue();
                        //Le pone los valores nuevos
                        socio.setDni(vista.sociosDni.getText());
                        socio.setNombre(vista.sociosNombre.getText());
                        socio.setFechaNacimiento(vista.sociosFechaNacimiento.getDate());
                        socio.setFechaRegistro(vista.sociosFechaRegistro.getDate());
                        //manda al objeto al mmétodo modificar socio
                        modelo.modificarSocio(socio);
                        //Lista los socios en la lista y en el combo box
                        listarSocios(modelo.getSocios());


                    }catch(NumberFormatException ne){
                        Util.showErrorAlert("Introduce números en los campos que lo requieran");
                    }

                }else{
                    Util.showErrorAlert("no puedes dejar campos vacíos");
                }
                break;
                //Elimina el socio
            case "socElim":
                //Crea un nuevo objeto de socio y le introduce el socio seleccionado en la lista
                Socio socio = vista.listaSocios.getSelectedValue();
                //Borra el objeto
                modelo.borrarSocio(socio);
                //Lista
                listarSocios(modelo.getSocios());
                break;
                //Método que añade una nueva compra
            case "comAniadir":
                if (!vista.comprasFecha.getText().equals("") || !vista.comprasPrecio.getText().equals("") ||
                        !vista.comprasProducto.getText().equals("") || vista.comprasSocios.getSelectedIndex()==-1){
                    try {

                    //Crea un nuevo objeto de compra
                    Compra compra = new Compra();
                    //Introduce los datos de compra
                        compra.setFechaCOmpra(vista.comprasFecha.getDate());
                        compra.setProducto(vista.comprasProducto.getText());
                        compra.setPrecio(vista.comprasPrecio.getText());
                        compra.setSocio((Socio) vista.comprasSocios.getSelectedItem());
                        //Manda el objeto al método que lo introduce en la base de datos
                    modelo.guardarCompra(compra);
                    //Lista las compras en la lista
                    listarCompras(modelo.getCompras());


                }catch(NumberFormatException ne){
                Util.showErrorAlert("Introduce números en los campos que lo requieran");
            }

                }else{
                    Util.showErrorAlert("no puedes dejar campos vacíos");
                }
                break;
                //Método que modifica una compra
            case "comMod":
                //Comprueba que los campos no estén vacíos
                if (!vista.comprasFecha.getText().equals("") || !vista.comprasPrecio.getText().equals("") ||
                        !vista.comprasProducto.getText().equals("") || vista.comprasSocios.getSelectedIndex()==-1){
                    try {

                        //Crea un nuevo objeto de compra y le añade el objeto que está seleccionado en la lista
                        Compra compra = vista.listaCompras.getSelectedValue();
                        //Le añade todos los datos
                        compra.setFechaCOmpra(vista.comprasFecha.getDate());
                        compra.setProducto(vista.comprasProducto.getText());
                        compra.setPrecio(vista.comprasPrecio.getText());
                        compra.setSocio((Socio) vista.comprasSocios.getSelectedItem());
                        //Manda al objeto de compra al método que lo modifica en la base de datos
                        modelo.modificarCompra(compra);
                        //Lista las compras en la lista
                        listarCompras(modelo.getCompras());


                    }catch(NumberFormatException ne){
                        Util.showErrorAlert("Introduce números en los campos que lo requieran");
                    }

                }else{
                    Util.showErrorAlert("no puedes dejar campos vacíos");
                }
                break;
                //Método que elimina una compra
            case "comELim":
                //Crea un nuevo objeto de compra y le añade el objeto que está seleccionado en la lista
                Compra compra = vista.listaCompras.getSelectedValue();
                //Manda el objeto al método que lo borra de la base de datos
                modelo.borrarCompra(compra);
                //Lista las compras
                listarCompras(modelo.getCompras());
                break;
        }





    }

    //Método que limpia la lista de las compras y la rellena con un List<> de compras
    private void listarCompras(List<Compra> compras) {
        vista.dlmCOmpras.clear();
        for (Compra compra : compras){
            vista.dlmCOmpras.addElement(compra);
        }
    }
    //Método que limpia la lista de los socios y la rellena con un List<> de socios
    private void listarSocios(List<Socio> socios) {
        vista.dlmSocios.clear();
        for (Socio socio : socios){
            vista.dlmSocios.addElement(socio);
            vista.comprasSocios.addItem(socio);
        }
    }



}

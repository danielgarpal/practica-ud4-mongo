package com.danielgarpal.gui;

import com.danielgarpal.base.Compra;
import com.danielgarpal.base.Socio;
import com.danielgarpal.util.Util;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Modelo {
    private MongoClient client;
    private MongoDatabase baseDatos;
    private MongoCollection coleccionSocios;
    private MongoCollection coleccionCompras;

    //Método que conecta con la base de datos
    public void conectar() {
        client=new MongoClient();
        baseDatos=client.getDatabase("cafeteriaMongo");
        coleccionSocios=baseDatos.getCollection("Socios");
        coleccionCompras=baseDatos.getCollection("Compras");
    }

    public void desconectar() {
        client.close();
    }

    //Método que recibe un socio y lo guarda en la base de datos
    public void guardarSocio(Socio socio) {
       coleccionSocios.insertOne(socioToDocument(socio));




    }

    //Método que recibe un socio, lo convierte a documentos de mongo y lo devuelve
    private Document socioToDocument(Socio socio) {
        Document documento = new Document();
        documento.append("nombre",socio.getNombre());
        documento.append("dni",socio.getDni());
        documento.append("fechaNacimiento", Util.formatearFecha(socio.getFechaNacimiento()));
        documento.append("fechaRegistro",Util.formatearFecha(socio.getFechaRegistro()));
        return documento;
    }
    //Método que recibe una compra, la convierte a documentos de Mongo y la devuelve
    private Document compraToDocument(Compra compra) {
        Document documento = new Document();
        documento.append("fechaCompra",Util.formatearFecha(compra.getFechaCOmpra()));
        documento.append("producto",compra.getProducto());
        documento.append("precio",compra.getPrecio());
        documento.append("socio",socioToDocument(compra.getSocio()));
        return documento;
    }

    //Método que añade los documentos de mongo al List<> de socios
    public List<Socio> getSocios(){
        ArrayList<Socio> lista = new ArrayList<>();
        Iterator<Document> it = coleccionSocios.find().iterator();
        while(it.hasNext()){
            lista.add(documentToSocio(it.next()));
        }
            return lista;
    }
    //Método que convierte los documentos de mongo a la clase Socio
    private Socio documentToSocio(Document document) {
        Socio unSocio = new Socio();
        unSocio.setId(document.getObjectId("_id"));
        unSocio.setNombre(document.getString("nombre"));
        unSocio.setDni(document.getString("dni"));
        unSocio.setFechaNacimiento(Util.parsearFecha(document.getString("fechaNacimiento")));
        unSocio.setFechaRegistro(Util.parsearFecha(document.getString("fechaRegistro")));
        return unSocio;
    }
    //Método que recibe un objeto de compra y lo mete a la base de datos
    public void guardarCompra(Compra compra) {
        coleccionCompras.insertOne(compraToDocument(compra));

    }



    //Método que devuelve las compras de la base de datos
    public List<Compra> getCompras() {
        ArrayList<Compra> lista = new ArrayList<>();
        Iterator<Document> it = coleccionCompras.find().iterator();
        while(it.hasNext()){
            lista.add(documentToCompra(it.next()));
        }
        return lista;
    }

    //Método que converte documentos de mongo a la clase compra
    private Compra documentToCompra(Document document) {
        Compra unaCompra = new Compra();
        unaCompra.setId(document.getObjectId("_id"));
        unaCompra.setFechaCOmpra(Util.parsearFecha(document.getString("fechaCompra")));
        unaCompra.setProducto(document.getString("producto"));
        unaCompra.setPrecio(document.getString("precio"));
        unaCompra.setSocio(documentToSocio((Document) document.get("socio")));
        return unaCompra;
    }

    //Método que modifica un socio de la base de datos
    public void modificarSocio(Socio socio) {
    coleccionSocios.replaceOne(new Document("_id",socio.getId()),socioToDocument(socio));
    }
    //Método que borra un socio de la base de datos
    public void borrarSocio(Socio socio){
        coleccionSocios.deleteOne(socioToDocument(socio));

    }
    //Método que modifica una compra de la base de datos
    public void modificarCompra(Compra compra) {
        coleccionCompras.replaceOne(new Document("_id",compra.getId()),compraToDocument(compra));
    }
    //Método que borra una compra de la base de datos
    public void borrarCompra(Compra compra){
        coleccionCompras.deleteOne(compraToDocument(compra));

    }
}

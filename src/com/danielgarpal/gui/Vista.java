package com.danielgarpal.gui;

import com.danielgarpal.base.Compra;
import com.danielgarpal.base.Socio;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

public class Vista extends JFrame{
    public JTabbedPane tabbedPane1;
    public JPanel panel1;
    public JTextPane textPane1;
    public JTextField sociosNombre;
    public JTextField sociosDni;
    public DatePicker sociosFechaNacimiento;
    public DatePicker sociosFechaRegistro;
    public JButton comprasAniadir;
    public JButton comprasModificar;
    public JButton comprasEliminar;
    public JButton sociosAniadir;
    public JButton sociosModificar;
    public JButton sociosEliminar;



    public JList<Socio> listaSocios;
    DefaultListModel<Socio> dlmSocios;
    public JList<Compra> listaCompras;
    public JTextField comprasProducto;
    public JTextField comprasPrecio;
    public DatePicker comprasFecha;
    public JComboBox comprasSocios;
    DefaultListModel<Compra> dlmCOmpras;


    public Vista() {
        JFrame frame = new JFrame("Trabajo Mongo");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        iniciar();
        frame.setSize(new Dimension(700, 400));
        frame.setLocationRelativeTo(null);
    }

    private void iniciar() {
        ponerDLM();





    }

    private void ponerDLM() {
        dlmSocios = new DefaultListModel();
        listaSocios.setModel(dlmSocios);
        dlmCOmpras = new DefaultListModel();
        listaCompras.setModel(dlmCOmpras);
    }


}
